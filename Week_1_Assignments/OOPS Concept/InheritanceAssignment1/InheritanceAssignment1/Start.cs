﻿  
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Text;  
using System.Threading.Tasks;  
using System.Data;  
using System.Collections;

namespace InheritanceAssignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                Console.WriteLine("Enter the Number of Employess");
                int result = int.Parse(Console.ReadLine());
                List<Employee> employees = new List<Employee>();
                for (int index = 1; index <= result; index++)
                {
                    Employee employee = new Employee();
                    Console.WriteLine("Enter the name of the Employee");
                    employee.Name = Console.ReadLine();
                    if (int.TryParse(employee.Name, out int store))

                    {
                        Console.WriteLine("Enter the value in string format");
                        return;
                    }

                    Console.WriteLine("Enter the Address of the Employee");
                    employee.Address = Console.ReadLine();
                    if (int.TryParse(employee.Address, out int output))

                    {
                        Console.WriteLine("Enter the value in string format");
                        return;
                    }


                    Console.WriteLine("Enter 1 for FullTimeEmployee 2 for PartTimeEmployee");
                    int choice = int.Parse(Console.ReadLine());

                    if (choice == 1)
                    {

                        FullTimeEmployee fullTimeEmployee = new FullTimeEmployee();
                        employee.salary = fullTimeEmployee.FullTimeEmployeeSalary();


                    }
                    else if (choice == 2)
                    {
                        PartTimeEmployee partTimeEmployee = new PartTimeEmployee();
                        employee.salary = partTimeEmployee.PartTimeEmployeeSalary();

                    }
                    else
                    {
                        Console.WriteLine("Enter the correct input");
                    }
                    employees.Add(employee);
                }
                                                            
                foreach (Employee list in employees)
                {
                    Console.WriteLine("Name : " + list.Name);
                    Console.WriteLine("Address : " + list.Address);
                    Console.WriteLine("Salary : " + list.salary);
                }
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                Console.WriteLine("Enter the correct input Error : " + message);
            }
        }
    } 
}

