﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphismAssignment2
{
    public class CallingArea
    {

        public void Area( int choice,double side,double width,double length,double baseoftriangle,double height,double radius)
        {
            CalculateArea sqaure = new Square();
            CalculateArea rectangle = new Rectangle();
            CalculateArea triangle = new Triangle();
            CalculateArea circle = new Circle();
            
            if (choice == 1)
            {
                sqaure.Area(side);
                sqaure.ShowResult();
            }
            else if (choice == 2)
            {
                rectangle.Area(length, width);
                rectangle.ShowResult();
            }
            else if (choice == 3)
            {
                triangle.Area(height, baseoftriangle);
                triangle.ShowResult();
            }
            else
            {
                circle.Area(radius);
                circle.ShowResult();
            }
        }
    }
}
