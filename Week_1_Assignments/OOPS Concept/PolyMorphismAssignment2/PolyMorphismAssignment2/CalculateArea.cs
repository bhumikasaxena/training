﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphismAssignment2
{
    class CalculateArea
    {
        public double result;
        public virtual void Area(double side)
        {
        }
        public virtual void Area(double length, double width)
        {
        }
        public void ShowResult()
        {
            Console.WriteLine("Your Result is "+ result);
        }
    }
}
