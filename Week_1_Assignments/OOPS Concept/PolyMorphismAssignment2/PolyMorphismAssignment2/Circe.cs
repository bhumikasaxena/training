﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphismAssignment2
{
    class Circle : CalculateArea
    {
        public override void Area(double radius)
        {
            result = 3.14159 * radius * radius;

        }
    }
}

