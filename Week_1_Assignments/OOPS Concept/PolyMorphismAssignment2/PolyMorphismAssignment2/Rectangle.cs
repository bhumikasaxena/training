﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphismAssignment2
{
    class Rectangle : CalculateArea
    {
        public override void Area(double length, double width)
        {
            result = length * width;
        }
    }
}
