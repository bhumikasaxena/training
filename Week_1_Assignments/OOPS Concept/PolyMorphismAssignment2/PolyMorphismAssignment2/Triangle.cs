﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphismAssignment2
{
    class Triangle : CalculateArea
    {
        public override void Area(double height, double baseoftriangle)
        {
            result = (height * baseoftriangle) / 2;
        }
    }
}
