﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphismAssignment2
{
    class Square : CalculateArea
    {

        public override void Area(double side)
        {
            result = side * side;
        }

    }
}
