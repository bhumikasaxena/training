﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncapsulationAssignment3
{
    public class AccountDetails
    {    
        private string accoutName;

        public string AccoutName
        {
            get
            {
                return accoutName;
            }
            set
            {
                accoutName = value;
            }
        }
        private string address = "India";
        // readonly property
        public string Address
        {
            get
            {
                return address;
            }
        }
        private string phone = "1234567890";
        // writeonly property
        public string Phone
        {
            set
            {
                phone = value;
            }
        }
    
        public void Function()
        {
            AccountDetails accountDetails = new AccountDetails();
            Console.WriteLine("enter the total number of customers");
            int result = int.Parse(Console.ReadLine());
            List<AccountDetails> accountDetail = new List<AccountDetails>();
            for (int index = 0; index < result; index++)
            {
                Console.WriteLine("Enter the name of the account");
                accountDetails.accoutName = Console.ReadLine();
                Console.WriteLine("Enter the phone number");
                accountDetails.phone = Console.ReadLine();
                address = accountDetails.address;
                accountDetail.Add(accountDetails);
            }
            foreach(AccountDetails account in accountDetail)
            {
                Console.WriteLine(accountDetails.accoutName);
                Console.WriteLine(accountDetails.phone);
                Console.WriteLine(accountDetails.address);
            }

        }

    }
}
