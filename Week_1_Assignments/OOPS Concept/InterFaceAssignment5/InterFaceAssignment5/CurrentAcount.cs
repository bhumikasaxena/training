﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterFaceAssignment4
{

    public class CurrentAccount : IBankAccount
    {
         decimal balance;
        public void Deposit(decimal amount)
        {
            balance += amount;
            return;
        }
        public void Withdraw(decimal amount)
        {
            if (balance < amount)
            {
                Console.WriteLine("Insufficient balance!");
                return;
            }
            else
            {
                balance -= amount;
                Console.WriteLine(String.Format("Successfully withdraw: {0,6:C}", amount));

                return;
            }
        }
        public void Show()
        {
            Console.WriteLine("Current Account Balance " + balance);
        }
    }
}
    

