﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterFaceAssignment4
{
   public class SavingsAccount : IBankAccount
   {
        decimal balance;
        decimal perDayLimit;
        public void Deposit(decimal amount)
        {
            balance += amount;           
        }
        public void Withdraw(decimal amount)
        {
            if (balance < amount)
            {
                Console.WriteLine("Insufficient balance!");
                return;
            }
            else if (perDayLimit + amount > 5000) //limit is 5000
            {
                Console.WriteLine("Withdrawal attempt failed!");
                return ;
            }
            else
            {
                balance -= amount;
                perDayLimit += amount;
                Console.WriteLine("Successfully withdraw" + amount);

                return ;
            }
        }
      

       public void  Show()
        {
            Console.WriteLine("Amount:"+balance);
        }
    }
}
