﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterFaceAssignment4
{
    public class UserInput
    {
        public void UsersInput()
        {
            try {
                while (true)
                {
                    Console.WriteLine("Enter 1 to Withdraw and 2 to Deposit money and 3 for exit");
                    int result = int.Parse(Console.ReadLine());
                    if (result == 1)
                    {
                        Console.WriteLine("Press 1 to withdraw money from Current Account and 2 to withdraw money from savings account");
                        int output = int.Parse(Console.ReadLine());
                        if (output == 1)
                        {
                            decimal amount;
                            CurrentAccount currentAccount = new CurrentAccount();
                            currentAccount.Deposit(5000);
                            Console.WriteLine("Enter the amount you want to withdarw");
                            amount = decimal.Parse(Console.ReadLine());
                            currentAccount.Withdraw(amount);
                            currentAccount.Show();
                        }
                        else if (output == 2)
                        {
                            decimal amount;
                            SavingsAccount savingsAccount = new SavingsAccount();
                            savingsAccount.Deposit(5000);
                            Console.WriteLine("Enter the amount you want to withdarw");
                            amount = decimal.Parse(Console.ReadLine());
                            savingsAccount.Withdraw(amount);
                            savingsAccount.Show();

                        }
                        else
                        {
                            Console.WriteLine("Enter the correct input");
                        }
                    }
                    else if (result == 2)
                    {
                        Console.WriteLine("Press 1 to Deposit money to Current Account and 2 to deposit money to savings account");
                        int output = int.Parse(Console.ReadLine());
                        if (output == 1)
                        {
                            decimal amount;
                            CurrentAccount currentAccount = new CurrentAccount();
                            currentAccount.Deposit(5000);
                            Console.WriteLine("Enter the amount you want to deposit");
                            amount = decimal.Parse(Console.ReadLine());
                            currentAccount.Deposit(amount);
                            currentAccount.Show();
                        }
                        else if (output == 2)
                        {
                            decimal amount;
                            SavingsAccount savingsAccount = new SavingsAccount();
                            savingsAccount.Deposit(5000);
                            Console.WriteLine("Enter the amount you want to deposit");
                            amount = decimal.Parse(Console.ReadLine());
                            savingsAccount.Deposit(amount);
                            savingsAccount.Show();

                        }
                    }
                    else if (result == 3)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Enter the correct input");
                    }
                }
            }
            catch(Exception exception)
            {
                string message = exception.Message;
                Console.WriteLine("Enter the coorect input ! ERROR :"+message);
            }
            
        }
    }
}
