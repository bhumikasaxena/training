﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstaractionAssignment4
{
    class Start
    {
        static void Main(string[] args)
        {
            FULLTimeEmployeeShow fULLTimeEmployeeShow = new FULLTimeEmployeeShow();
            PartTimeEmployeeShow partTimeEmployeeShow = new PartTimeEmployeeShow();
            try {
                Console.WriteLine("Enter the number of employees");
                int result = int.Parse(Console.ReadLine());
                for (int count = 0; count < result; count++)
                {
                    Console.WriteLine("press 1 for full time employee and 2 for part time employee");
                    int output = int.Parse(Console.ReadLine());
                    if (output == 1)
                    {
                        fULLTimeEmployeeShow.DisplayEmployee(result);
                    }
                    else if (output == 2)
                    {
                        partTimeEmployeeShow.DisplayEmployess(result);
                    }
                }
            }
            catch(Exception exception)
            {
                string messsage = exception.Message;
                Console.WriteLine("Enter the input in correct Format"+messsage);
            }
         }
    }
}
