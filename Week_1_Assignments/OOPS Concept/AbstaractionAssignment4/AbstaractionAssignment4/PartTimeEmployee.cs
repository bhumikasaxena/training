﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstaractionAssignment4
{
    public class PartTimeEmployee : Employee
    {
        public int HourlyPay { get; set; }
        public int TotalHoursWorked { get; set; }

        public override int GetMonthlySalary()
        {
            return HourlyPay * TotalHoursWorked;
        }
    }
}
